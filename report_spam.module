<?php

/**
 * @file
 * Allows for crowdsourced spam reporting.
 */

/**
 * Implements hook_link().
 */
function report_spam_link($type, $content = 0, $main = 0) {
  switch ($type) {
    case 'comment':
      return report_spam_links($type, $content->cid, $content);
      break;
    case 'node':
      return report_spam_links($type, $content->nid, $content);
      break;
  }
}

/**
 * Implements hook_link_alter().
 */
function report_spam_link_alter(&$links, $node, $comment = NULL) {
  if (!user_access('administer spam')) {
    if (isset($links['spam'])) {
      unset($links['spam']);
    }
    if (isset($links['mark-as-not-spam'])) {
      unset($links['mark-as-not-spam']);
    }
  }
}

/**
 * Add the appropriate links to all content that is actively being filtered.
 */
function report_spam_links($type, $id, $content) {
  global $user;

  $links = array();
  if (spam_invoke_module($type, 'filter_content_type', $content)) {
    if (user_access('mark content as spam')) {
      $score = (int)db_result(db_query("SELECT score FROM {spam_tracker} WHERE content_type = '%s' AND content_id = '%s'", $type, $id));
      $already_marked = (int)db_result(db_query("SELECT 1 FROM {report_spam_log} WHERE content_type = '%s' AND content_id = '%s' AND uid = '%s'", $type, $id, $user->uid));

      if (!spam_score_is_spam($score) && !(bool)$already_marked) {
        $token = drupal_get_token("report spam $type $id");
        $links['report-spam'] = array('href' => "report_spam/$type/$id", 'title' => t('report spam'), 'query' => array('token' => $token));
      }
    }
  }
  return $links;
}

/**
 * Implements hook_menu().
 */
function report_spam_menu() {
  $items['report_spam/%/%'] = array(
    'page callback' => 'report_spam_callback',
    'page arguments' => array(1, 2, array('redirect' => TRUE)),
    'type' => MENU_CALLBACK,
    'access arguments' => array('mark content as spam'),
  );
  return $items;
}

/**
 * Menu callback that is used for the not spam mark links.
 *
 * This is a wrapper with CSFR protection for spam_mark_as_spam
 */
function report_spam_callback($type, $id, $extra = array()) {
  if (drupal_valid_token($_GET['token'], "report spam $type $id")) {
    global $user;
    $weights = variable_get('report_spam_weights', array('authenticated user' => 5));

    // Use the highest weight that the user's roles can give.
    $weight = 0;
    foreach ($user->roles as $role) {
      if (array_key_exists($role, $weights) && $weights[$role] > $weight) {
        $weight = $weights[$role];
      }
    }

    $score = (int)db_result(db_query("SELECT score FROM {spam_tracker} WHERE content_type = '%s' AND content_id = '%s'", $type, $id));
    $score = $score + $weight;
    $extra['score'] = $score;

    // Record that the user reported the spam.
    $record = array(
      'content_type' => $type,
      'content_id' => $id,
      'uid' => $user->uid,
    );
    drupal_write_record('report_spam_log', $record);
    spam_mark_as_spam($type, $id, $extra);
  }
  else {
    return drupal_access_denied();
  }
}
